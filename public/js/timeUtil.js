/**
 * 时间转换2019-04-07T16:00:00.000+0000 到2019-04-07 16:00
 */
function timeConversion(date) {
    const arr=date.split("T");
    const d = arr[0];
    const t=arr[1];
    const lastIndexOf = t.lastIndexOf(':');
    const time = t.substring(0,lastIndexOf);
    return d+" " +time;
}

export {
    timeConversion
}