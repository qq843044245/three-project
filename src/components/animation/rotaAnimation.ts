import * as BABYLON from '@babylonjs/core/Legacy/legacy';
import {Animation} from "@babylonjs/core/Animations/animation";

const keys = [
    {frame: 0, value: 0},
    {frame: 30, value: Math.PI},
    {frame: 60, value: 2 * Math.PI},
    {frame: 90, value: 3 * Math.PI},
    {frame: 120, value: 4 * Math.PI},
];

/**
 * 旋转动画
 */
export class rotaAnimation {

    static getRotaAnimation(): Animation {
        let animation = new BABYLON.Animation("anima01", "rotation.y", 10,
            BABYLON.Animation.ANIMATIONTYPE_FLOAT,
            BABYLON.Animation.ANIMATIONLOOPMODE_CYCLE);
        animation.setKeys(keys);

        return animation;
    }

}
