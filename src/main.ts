import { createApp } from 'vue'
import App from './App.vue'
import vueRouter from './router/index'

const app =createApp(App)

app.use(vueRouter)
app.use(vant)
app.mount('#app')

//挂载全局属性
app.config.globalProperties.$pub_dir = import.meta.env.BASE_URL+import.meta.env.VITE_PUB_DIR

