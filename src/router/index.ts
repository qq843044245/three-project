import { createRouter,createWebHashHistory} from "vue-router";

  // 路由信息
  const routes = [
      {
          path: "/",
          redirect: "/home",
      },
      {
          path: "/baGua",
          name: "bagua",
          component:  () => import('../views/baGua.vue'),
      },
      {
          path: "/home",
          name: "home",
          component:  () => import('../views/home.vue'),
      },
  ];
  
  // 导出路由
  const router = createRouter({
      history: createWebHashHistory(),
      routes
  });
  
  export default router;
