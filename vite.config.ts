import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'

// const path = require('path')

export default defineConfig(  ({command,mode}) =>{
  const pa = mode === 'production' ? '/3d/' : '/';
  console.log("初始目录："+pa);

  return {
    plugins: [vue()],
    base: pa,
    server:{port:3000},

    // alias: {
    //   // 键必须以斜线开始和结束
    //   '/@/': path.resolve(__dirname, './src')
    // },
  }


})
